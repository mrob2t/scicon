package DB;

import java.io.*;

public class DocumentArticle {
    private final static String ARTICLES_LOCATION = "/Users/tuhva/Desktop/REPOSITORY/scicon/code/src/main/resources/articles/";

    public static String getArticleBodyById(int id) {
        String path = ARTICLES_LOCATION + id + ".html";
        try (BufferedReader br = new BufferedReader(new FileReader(new File(ARTICLES_LOCATION + id + ".html")))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) sb.append(line);
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void saveArticle(int id, String body) {
        String path = ARTICLES_LOCATION + id + ".html";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)))) {
            bw.write(body);
            bw.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}

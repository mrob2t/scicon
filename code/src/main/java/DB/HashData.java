package DB;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashData {
    private final byte[] SALT = "zyxwvutsrqponmlkjihgfedcba".getBytes();

    private String h(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
            md.update(SALT);
            byte[] newValue = md.digest(password.getBytes(StandardCharsets.UTF_8));

            return java.util.Base64.getEncoder().encodeToString(newValue);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return password;
    }

    public String hash(String value) {
        return h(value).substring(1,15);
    }
}

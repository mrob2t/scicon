package DB;

import repository.UserRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {
    public static Connection getConnection() {
        String url = "jdbc:mysql://localhost:3306/scion?serverTimezone=UTC";
        String user = "root";
        String password = "qwerty007";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        MySqlConnection.getConnection();
        UserRepository userRepository = new UserRepository();
    }

}

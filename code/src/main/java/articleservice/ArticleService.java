package articleservice;

import DB.DocumentArticle;
import model.Article;
import repository.ArticleRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleService {
    public ArrayList<Article> articles = new ArrayList<>();
    private ArticleRepository articleRepository = ArticleRepository.newInstance();

    public Article getArticleById(int id) {
        return articleRepository.find(id);
    }

    public static void main(String[] args) {
        ArticleService articleService = new ArticleService();
        articleService.delete_id(1);
    }

    public int getArticlesSize() {
        return articleRepository.getArticle_id();
    }

    public ArrayList<Article> getArticles() {
        int n = articleRepository.getArticle_id();
        for (int id = 0; id < n; id++) {
            Article article = getArticleById(id);
            if (article != null) {
                articles.add(article);
            }
        }
        return articles;
    }

    public void addArticle(Article article) {
        articleRepository.add(article);
    }

    private List<Integer> getArticlesCategory(String category) {
        int id = articleRepository.getCategory_id(category);
        return articleRepository.getSQL_FIND_ALL_CATEGORY_TO_ID(id);
    }

    public List<Article> getArticlesByCategory(String category) {
        List<Integer> list = getArticlesCategory(category);
        List<Article> articles = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            articles.add(getArticleById(list.get(i)));
        }
        return articles;
    }

    public void delete_id(int id){
        articleRepository.delete_id(id);
    }

    private final static String ARTICLES_LOCATION = "/Users/tuhva/Desktop/REPOSITORY/scicon/code/src/main/resources/articles/";

    public String getArticleBodyById(int id) {
        String path = ARTICLES_LOCATION + id + ".html";
        try (BufferedReader br = new BufferedReader(new FileReader(new File(ARTICLES_LOCATION + id + ".html")))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) sb.append(line);
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void saveArticle(int id, String body) {
        String path = ARTICLES_LOCATION + id + ".html";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)))) {
            bw.write(body);
            bw.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}

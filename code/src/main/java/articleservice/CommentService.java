package articleservice;

import model.Comment;
import repository.CommentRepository;
import repository.RepositoryDAO;

import java.util.ArrayList;
import java.util.List;

public class CommentService {
    CommentRepository commentRepository = new CommentRepository();

    public void addComment(Comment comment) {
        commentRepository.add(comment);
    }

    private ArrayList<Comment> find(int articleId) {
        return commentRepository.found(articleId);
    }

    public void delete_id(int articleId){
        commentRepository.delete_id(articleId);
    }

    public ArrayList<Comment> getCommentsByArticleId(int articleId) {
        return find(articleId);
    }

}

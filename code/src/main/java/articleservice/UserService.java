package articleservice;

import model.User;
import repository.RepositoryUser;
import repository.UserRepository;

public class UserService {
    private final RepositoryUser repositoryUser = new UserRepository();

    public boolean add(User user) {
        return repositoryUser.add(user);
    }

    public User find(int id){return repositoryUser.find(id);}

    public int priority(int id) {
        return repositoryUser.priority(id);
    }
}

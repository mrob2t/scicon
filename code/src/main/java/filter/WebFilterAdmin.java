package filter;

import articleservice.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebFilter("/delete")
public class WebFilterAdmin implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        boolean time = false;
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals("time")) {
                time = true;
                break;
            }
        }

        if (request.getSession(false) != null && time) {
            System.out.println(123);
            int id = Integer.parseInt(request.getSession().getAttribute("id").toString());
            UserService userService = new UserService();
            if (userService.priority(id) == 2) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
        ((HttpServletResponse) servletResponse).sendRedirect("/home");
    }
}

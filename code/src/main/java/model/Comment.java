package model;

public class Comment {
    private int article_id;
    private int author_id;
    private String text;
    private String date;

    public Comment(int article_id, int author_id, String text, String date) {
        this.article_id = article_id;
        this.author_id = author_id;
        this.text = text;
        this.date = date;
    }
    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




}

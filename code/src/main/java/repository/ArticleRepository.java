package repository;

import DB.MySqlConnection;
import model.Article;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleRepository implements RepositoryDAO<Article> {

    private ArticleRepository() {
        update_id();
    }

    private static ArticleRepository articleRepository = null;

    public static ArticleRepository newInstance() {
        if (articleRepository == null) {
            articleRepository = new ArticleRepository();
        }
        return articleRepository;
    }

    private int article_id = 0;
    private final String SQL_COUNT_ROW
            = "select count(*) from ";

    private int update_id(String table) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement("select * from " + table);
            resultSet = statement.executeQuery();
            int k = 0;
            while (resultSet.next()) {
                int i = Integer.parseInt(resultSet.getString("article_id").toString());
                if (i > k) {
                    k = i;
                }
            }
            return k + 1;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getArticle_id() {
        return update_id("ARTICLE");
    }

    private void update_id() {
        article_id = update_id("article");
    }

    private final String SQL_FIND_ARTICLE
            = "SELECT * FROM ARTICLE WHERE article_id = ?";

    public static void main(String[] args) {
        ArticleRepository articleRepository = new ArticleRepository();
        articleRepository.delete_id(2);
    }

    public Article find(int id) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ARTICLE);
            statement.setString(1, String.valueOf(id));
            resultSet = statement.executeQuery();
            resultSet.next();

            Article article = new Article();
            article.setTitle(resultSet.getString("title_text"));
            System.out.println(resultSet.getString("category_id"));
            String category = getSQL_FIND_CATEGORY_TO_ID(Integer.parseInt(resultSet.getString("category_id")));
            String authorId = getSQL_FIND_USER(Integer.parseInt(resultSet.getString("user_id")));

            String description = resultSet.getString("description_text");
            String date = resultSet.getString("data");

            article.setId(Integer.parseInt(resultSet.getString("article_id")));

            article.setCategory(category);
            article.setDate(date);
            article.setDescription(description);
            article.setAuthorId(Integer.parseInt(authorId));

            return article;
        } catch (SQLException e) {
            return null;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_ADD_ARTICLE
            = "INSERT INTO ARTICLE(ARTICLE_id,title_text,description_text,user_id,category_id,data) VALUES(?,?,?,?,?,?)";

    public boolean add(Article article) {
        update_id();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_ADD_ARTICLE);

            int category_id = getCategory_id(article.getCategory());

            int user_id = article.getAuthorId();
            String title_text = article.getTitle();
            String description = article.getDescription();
            String data = article.getDate();
            statement.setString(1, String.valueOf(article_id));
            statement.setString(2, title_text);
            statement.setString(3, description);
            statement.setString(4, String.valueOf(user_id));
            statement.setString(5, String.valueOf(category_id));
            statement.setString(6, data);

            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_FIND_CATEGORY_TO_TEXT
            = "SELECT category_id FROM category WHERE category_text = ?";

    public int getCategory_id(String category) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_CATEGORY_TO_TEXT);
            statement.setString(1, category);
            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("category_id");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private final String SQL_FIND_BY_CATEGORY_TO_ID
            = "SELECT * FROM CATEGORY WHERE category_id = ?";

    private String getSQL_FIND_CATEGORY_TO_ID(int id) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_CATEGORY_TO_ID);
            statement.setString(1, String.valueOf(id));

            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getString("category_text");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_FIND_USER
            = "SELECT * FROM user WHERE id = ?";

    private String getSQL_FIND_USER(int id) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_USER);
            statement.setString(1, String.valueOf(id));
            resultSet = statement.executeQuery();
            resultSet.next();

            return resultSet.getString("id");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_FIND_ALL_CATEGORY_TO_ID
            = "SELECT * FROM ARTICLE WHERE category_id = ?";

    public List<Integer> getSQL_FIND_ALL_CATEGORY_TO_ID(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_CATEGORY_TO_ID);
            statement.setString(1, String.valueOf(id));

            resultSet = statement.executeQuery();
            List<Integer> listcategory = new ArrayList<>();
            while (resultSet.next())
                listcategory.add(Integer.parseInt(resultSet.getString("article_id")));

            return listcategory;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_DELETE_ID
            = "DELETE FROM article WHERE (article_id = ?)";

    public void delete_id(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_DELETE_ID);
            statement.setString(1, String.valueOf(id));
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

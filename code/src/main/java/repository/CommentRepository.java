package repository;


import DB.MySqlConnection;
import TimeData.DataTimeService;
import liquibase.pro.packaged.C;
import liquibase.pro.packaged.S;
import model.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentRepository implements RepositoryDAO<Comment> {

    private final String SQL_DELETE_COMMENT
            ="DELETE FROM COMMENT WHERE (article_id = ?)";

    public void delete_id(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_DELETE_COMMENT);
            statement.setString(1, String.valueOf(id));
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_FIND_COMMENT
            = "SELECT * FROM COMMENT WHERE article_id = ?";

    public ArrayList<Comment> found(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            ArrayList<Comment> comments = new ArrayList<>();
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_COMMENT);
            statement.setString(1, String.valueOf(id));
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int article_id = resultSet.getInt("article_id");
                int author_id = resultSet.getInt("author_id");
                String text = resultSet.getString("text");
                String date = resultSet.getString("date");
                comments.add(new Comment(article_id, author_id, text, date));
            }
            return comments;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_ADD_COMMENT
            = "INSERT INTO COMMENT(article_id,author_id,text,date) VALUES(?,?,?,?)";

    @Override
    public boolean add(Comment comment) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_ADD_COMMENT);
            statement.setString(1, String.valueOf(comment.getArticle_id()));
            statement.setString(2, String.valueOf(comment.getAuthor_id()));
            statement.setString(3, comment.getText());
            statement.setString(4, comment.getDate());
            int a = statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public Comment find(int id) {
        return null;
    }

    public static void main(String[] args) {
        CommentRepository commentRepository = new CommentRepository();

        // commentRepository.add(new Comment(1,13,"tezt",DataTimeService.time()));
        //System.out.println(commentRepository.found(1).toArray());
    }


}

package repository;

import model.User;

public interface RepositoryDAO<T> {
    public T find(int id);
    public boolean add(T t);
}

package repository;

import model.User;

public interface RepositoryUser extends RepositoryDAO<User> {
    public Integer find(String email,String password);
    public int priority(int id);
}

package repository;

import DB.HashData;
import DB.MySqlConnection;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository implements RepositoryUser {
    private final String SQL_FIND_USER
            = "SELECT id FROM USER WHERE email = ? and password = ? ";

    private final String SQL_ADD_USER
            = "INSERT INTO USER(name,lastname,email,password) VALUES(?,?,?,?)";

    private final String SQL_USER_ID
            = "SELECT * FROM USER WHERE ID = ?";

    private final String SQL_LEVEL_STATUS
            = "SELECT NAME FROM PRIORITY WHERE level = ?";

//    public static void main(String[] args) {
//        UserRepository userRepository = new UserRepository();
//        String a = userRepository.getSQL_LEVEL_STATUS(2);
//        System.out.println(a);
//    }

    public String getSQL_LEVEL_STATUS(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_LEVEL_STATUS);

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getString("name");
        } catch (Exception e) {
            return null;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public User find(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_USER_ID);

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            ResultSet row = resultSet;
            row.next();
            int level = row.getInt("id_level");
            String status = getSQL_LEVEL_STATUS(level);
            User user = new User(row.getInt("id"), row.getString("name"), row.getString("lastname"), row.getString("email"), row.getString("password"),status);
            return user;

        } catch (Exception e) {
            return null;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean add(User user) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_ADD_USER);
            statement.setString(1, user.getName());
            statement.setString(2, user.getLastname());
            statement.setString(3, user.getEmail());
            statement.setString(4, hash(user.getPassword()));
            int a = statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String hash(String hash) {
        HashData hashData = new HashData();
        return hashData.hash(hash);
    }

    public Integer find(String email, String password) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_FIND_USER);

            statement.setString(1, email);

            password = hash(password);
            statement.setString(2, password);

            resultSet = statement.executeQuery();
            resultSet.next();

            return resultSet.getInt("id");

        } catch (Exception e) {
            return null;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final String SQL_USER_PRORITY
            = "SELECT id_level from user where id = ?";
    @Override
    public int priority(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = MySqlConnection.getConnection();
            statement = connection.prepareStatement(SQL_USER_PRORITY);
            statement.setString(1,String.valueOf(id));
            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("id_level");

        } catch (Exception e) {
            throw  new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

package web;

import TimeData.DataTimeService;
import articleservice.CommentService;
import articleservice.UserService;
import model.Comment;
import model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

@WebServlet("/article")
public class ArticleAdd extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Object userId = req.getSession().getAttribute("id");
        if (userId != null) {
            User user = new UserService().find((int) userId);
            req.setAttribute("user_id", userId);
            req.setAttribute("status", user.getStatus());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/article.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader br = req.getReader();
        StringBuilder text = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) text.append(line);

        int authorId = Integer.parseInt(req.getParameter("author_id"));
        int articleId = Integer.parseInt(req.getParameter("article_id"));

        CommentService commentService = new CommentService();

        Comment comment = new Comment(articleId, authorId, text.toString(), DataTimeService.time().toString());

        commentService.addComment(comment);
    }
}

package web;

import articleservice.ArticleService;
import repository.CommentRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")
public class ArticleDelete extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        int id = Integer.parseInt(req.getParameter("id"));
        ArticleService articleService = new ArticleService();
        CommentRepository commentRepository = new CommentRepository();
        commentRepository.delete_id(id);
        articleService.delete_id(id);
    }
}

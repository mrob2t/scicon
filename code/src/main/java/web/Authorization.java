package web;

import repository.RepositoryUser;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/login")
public class Authorization extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String remeber = req.getParameter("rememberme");

        Cookie cookie = new Cookie("time", "time");
        cookie.setMaxAge(-1);

        if (remeber != null) {
            cookie.setMaxAge(12960000);
        }

        RepositoryUser repository = new UserRepository();
        Integer id = repository.find(email, password);

        if (id != null) {

            HttpSession session = req.getSession();
            session.setAttribute("id", id);

            resp.addCookie(cookie);
            resp.sendRedirect("/profile");
        } else {
            resp.sendRedirect("/register");
        }
    }
}

package web;

import TimeData.DataTimeService;
import articleservice.ArticleService;
import model.Article;
import repository.ArticleRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;

@WebServlet("/new-article")
public class NewArticle extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/new-article.html");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Article article = new Article();

        article.setTitle(req.getParameter("title"));
        article.setDescription(req.getParameter("description"));
        article.setCategory(req.getParameter("category"));
        article.setDate(DataTimeService.time());
        article.setAuthorId(Integer.parseInt(req.getSession().getAttribute("id").toString()));

        ArticleService articleService = new ArticleService();

        BufferedReader br = req.getReader();
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) sb.append(line);

        articleService.saveArticle(articleService.getArticlesSize(), sb.toString());
        articleService.addArticle(article);

        int id = articleService.getArticlesSize() - 1;
        resp.getWriter().write("/article?id=" + id);
    }

    public static void main(String[] args) {
        ArticleRepository articleRepository = ArticleRepository.newInstance();
        System.out.println(articleRepository.find(0));
    }
}

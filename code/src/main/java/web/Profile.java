package web;

import articleservice.UserService;
import model.User;
import repository.RepositoryDAO;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/profile")
public class Profile extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        Integer id = (Integer)session.getAttribute("id");

        UserService userService = new UserService();
        User user = userService.find(id);

        req.setAttribute("name",user.getName());
        req.setAttribute("lastname",user.getLastname());
        req.setAttribute("email",user.getEmail());
        req.setAttribute("status",user.getStatus());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profile.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

}

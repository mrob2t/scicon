package web;

import articleservice.UserService;
import model.User;
import repository.RepositoryDAO;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/register")
public class Registrator extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/registration.html");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        System.out.println(1);
        String name = req.getParameter("name");
        String lastname = req.getParameter("lastname");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        User user = new User(name, lastname, email, password);

        UserService userService = new UserService();

        if (userService.add(user)) {
            resp.sendRedirect("/profile");
        } else {
            resp.sendRedirect("/register");
        }
    }

}

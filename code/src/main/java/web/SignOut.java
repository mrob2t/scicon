package web;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/signout")
public class SignOut extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        Cookie cookie = new Cookie("time","time");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        resp.sendRedirect("/home");
    }
}

-- liquibase formatted SQL
-- changeset BULAT:4
CREATE TABLE `article` (
  `article_id` int NOT NULL,
  `title_text` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `description_text` varchar(45) COLLATE utf8_bin NOT NULL,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `data` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `category_id_idx` (`category_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `description_id_idx` (`description_text`),
  KEY `main_text_id_idx` (`data`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

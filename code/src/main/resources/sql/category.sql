-- liquibase formatted SQL
-- changeset BULAT:2
CREATE TABLE `category` (
  `category_id` int NOT NULL,
  `category_text` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

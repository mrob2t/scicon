-- liquibase formatted SQL
-- changeset BULAT:5
CREATE TABLE `priority` (
  `level` int NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

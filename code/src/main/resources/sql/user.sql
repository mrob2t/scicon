-- liquibase formatted SQL
-- changeset BULAT:10
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `id_level` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_level_idx` (`id_level`),
  CONSTRAINT `id_level` FOREIGN KEY (`id_level`) REFERENCES `priority` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

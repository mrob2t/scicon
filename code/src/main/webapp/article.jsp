<%@ page import="model.Article" %>
<%@ page import="articleservice.ArticleService" %>
<%@ page import="TimeData.DataTimeService" %>
<%@ page import="articleservice.CommentService" %>
<%@ page import="model.Comment" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="model.User" %>
<%@ page import="articleservice.UserService" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<% Integer articleId = Integer.parseInt(request.getParameter("id")); %>
<% ArticleService articleService = new ArticleService(); %>
<% Article article = articleService.getArticleById(articleId); %>
<% Integer userId = (Integer) request.getAttribute("user_id");%>
<% String userStatus = (String) request.getAttribute("status");%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./styles/global.css">
    <link rel="stylesheet" href="./styles/article.css">
    <title><%= article.getTitle() %>
    </title>
    <script type="text/javascript" src="js/deleteArticle.js"></script>
    <script type="text/javascript" src="js/saveComment.js"></script>
    <script type="text/javascript"
            src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
</head>
<body>
<div class="menu" id="menu">
    <a href="${pageContext.request.contextPath}/home">Главная</a>
    <a href="${pageContext.request.contextPath}/category">Категории</a>
    <div class="account" id="account">
        <% if (userId == null) { %>
        <a href="${pageContext.request.contextPath}/login">Войти</a>
        <a href="${pageContext.request.contextPath}/register">Регистрация</a>
        <% } else { %>
        <a href="${pageContext.request.contextPath}/profile">Личный кабинет</a>
        <% } %>
    </div>
    <a href="javascript:void(0);" class="icon" onclick="toggleDropdownMenu()"><i class="fa fa-bars"></i></a>
    <script type="text/javascript" src="js/toggleDropdownMenu.js"></script>
</div>
<div class="article">
    <h5><%= article.getCategory() %>
    </h5>
    <h5 class="date"><%= article.getDate() %>
    </h5>
    <h1><%= article.getTitle() %>
    </h1>
    <div id="article-body">
        <%= articleService.getArticleBodyById(articleId)%>
    </div>
    <% if (userStatus != null && userStatus.equals("администратор")) { %>
    <div>
        <button class="delete" onclick="deleteArticle(<%= articleId %>)">Удалить</button>
    </div>
    <% } %>

    <h3>Комментарии</h3>
    <% if (userId != null) { %>
    <div>
        <textarea id="comment" placeholder="Ваш комментарий"></textarea>
        <button onclick="saveComment(<%= userId %>, <%= articleId %>)">Выложить</button>
    </div>
    <% } %>

    <% CommentService commentService = new CommentService();%>
    <% ArrayList<Comment> comments = commentService.getCommentsByArticleId(articleId); %>
    <% for (int i = comments.size() - 1; i >= 0; i--) { %>
    <% Comment comment = comments.get(i); %>
    <%User author = new UserService().find(comment.getAuthor_id());%>
    <h5><%= author.getName() + " " + author.getLastname() %>
    </h5>
    <h5 class="date"><%= comment.getDate() %>
    </h5>
    <p class="comment-body"><%= comment.getText() %>
    </p>
    <% } %>
</div>
</body>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./styles/global.css">
    <title>Категории</title>
</head>
<body>
<div class="menu" id="menu">
    <a href="${pageContext.request.contextPath}/home">Главная</a>
    <a href="${pageContext.request.contextPath}/category" class="active">Категории</a>
    <div class="account" id="account">
        <% if (session.getAttribute("id") == null) { %>
        <a href="${pageContext.request.contextPath}/login">Войти</a>
        <a href="${pageContext.request.contextPath}/register">Регистрация</a>
        <% } else { %>
        <a href="${pageContext.request.contextPath}/profile">Личный кабинет</a>
        <% } %>
    </div>
    <a href="javascript:void(0);" class="icon" onclick="toggleDropdownMenu()"><i class="fa fa-bars"></i></a>
</div>
<div style="margin-left: 16px; margin-right: 16px;">
    <p><a href="${pageContext.request.contextPath}/category?name=Астрономия">Астрономия</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=Биология">Биология</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=География">География</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=Лингвистика">Лингвистика</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=Математика">Математика</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=Физика">Физика</a></p>
    <p><a href="${pageContext.request.contextPath}/category?name=Химия">Химия</a></p>
</div>
</body>
</html>
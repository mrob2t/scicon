<%@ page import="articleservice.ArticleService" %>
<%@ page import="model.Article" %>
<%@ page import="java.util.List" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<% String categoryName = URLDecoder.decode(request.getParameter("name"), StandardCharsets.UTF_8.name());%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles/global.css">
    <link rel="stylesheet" href="styles/homepage.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="js/toggleDropdownMenu.js"></script>
    <title><%=categoryName%></title>
</head>
<body>
<div class="menu" id="menu">
    <a href="${pageContext.request.contextPath}/home">Главная</a>
    <a href="${pageContext.request.contextPath}/category" class="active">Категории</a>
    <div class="account" id="account">
        <% if (session.getAttribute("id") == null) { %>
        <a href="${pageContext.request.contextPath}/login">Войти</a>
        <a href="${pageContext.request.contextPath}/register">Регистрация</a>
        <% } else { %>
        <a href="${pageContext.request.contextPath}/profile">Личный кабинет</a>
        <% } %>
    </div>
    <a href="javascript:void(0);" class="icon" onclick="toggleDropdownMenu()"><i class="fa fa-bars"></i></a>
</div>
<div class="article-previews">
    <% ArticleService articleService = new ArticleService();%>
    <% List<Article> articles = articleService.getArticlesByCategory(categoryName); %>
    <% for (int i = articles.size() - 1; i >= 0; i--) { %>
    <% Article article = articles.get(i); %>
    <h5><%= article.getCategory() %>
    </h5>
    <h5 class="date"><%= article.getDate() %>
    </h5>
    <h2><%= article.getTitle() %>
    </h2>
    <p><%= article.getDescription() %>
    </p>
    <a href="${pageContext.request.contextPath}/article?id=<%=article.getId()%>">Читать</a>
    <% } %>
</div>
</body>
function deleteArticle(id) {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:8080/delete?id=' + id, false);
    xhr.send();
    if (xhr.status === 200) {
        window.location = 'http://localhost:8080/home';
    }
}
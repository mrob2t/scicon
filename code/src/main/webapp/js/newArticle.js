function saveArticle() {
    const xhr = new XMLHttpRequest();
    const title = document.getElementById("title")
    const description = document.getElementById("description")
    const selector = document.getElementById("selector")
    const editor = document.getElementById("editor")
    xhr.open('POST', 'http://localhost:8080/new-article?title=' + title.value + "&description=" + description.value + "&category=" + selector.value, false);
    xhr.send(editor.innerHTML);
    if (xhr.status === 200) {
        const articleUrl = xhr.responseText;
        window.location = 'http://localhost:8080' + articleUrl;
    }
}

function format(command, value) {
    document.execCommand(command, false, value);
}

function setUrl() {
    const url = prompt("Введите URL:", "https://");
    const text = document.getSelection();
    if (url != null) document.execCommand('insertHTML', false, '<a contenteditable="false" href="' + url + '" target="_blank">' + text + '</a>');
}

function insertImage() {
    const url = prompt("Введите URL изображения:", "https://");
    if (url != null) document.execCommand('insertHTML', false, '<img class="centered" src="' + url + '"/>');
}

function insertVideo() {
    const id = prompt("Введите ID YouTube-видео:", "dQw4w9WgXcQ");
    if (id != null) document.execCommand('insertHTML', false, '<iframe class="centered" width="600" height="350" src="https://www.youtube.com/embed/' + id + '"></iframe>');
}

function previewMath() {
    let previewWindow = window.open("", "", "width=600,height=300");
    const text = document.getSelection();
    const content = `
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="./styles/global.css">
            <title>Предпросмотр выражения</title>
            <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"><\/script>
        </head>
        <body>
            ${text}
        </body>
        `
    previewWindow.document.write(content);
    previewWindow.document.close();
}
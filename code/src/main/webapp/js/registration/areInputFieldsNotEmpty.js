function areInputFieldsNotEmpty() {
    const inputs = document.getElementsByTagName('input');
    for (let i = 0; i < inputs.length; i++) {
        if (!inputs[i].value.toString().trim()) {
            return false;
        }
    }
    return true;
}
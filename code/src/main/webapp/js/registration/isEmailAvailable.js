function isEmailAvailable() {
    const xhr = new XMLHttpRequest();
    const error = document.getElementById("email_not_unique_error")
    const email = document.getElementById("email")
    xhr.open('GET', 'http://localhost:8080/test?email=' + email.value, false);
    xhr.send();
    if (xhr.status === 200) {
        if (xhr.responseText === 'false') {
            error.style.display = 'block';
        }
    }
}
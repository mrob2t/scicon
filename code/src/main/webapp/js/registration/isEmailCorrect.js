function isEmailCorrect() {
    document.getElementById('email_error').style.display = 'none';
    const email = document.getElementById('email');
    if (email.value !== "" && !email.value.toString().match(".{1,64}@.{1,255}")) {
        document.getElementById('email_error').style.display = 'block';
        return false;
    }
    return true;
}
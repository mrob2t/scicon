function isPasswordCorrect() {
    document.getElementById('password_error').style.display = 'none';
    const button = document.getElementById('button')
    const password = document.getElementById('password');
    if (password.value !== "" && password.value.toString().length < 6) {
        document.getElementById('password_error').style.display = 'block';
        return false;
    }
    return true;
}
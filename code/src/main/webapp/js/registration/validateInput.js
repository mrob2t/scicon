function validateInput() {
    const button = document.getElementById('button')
    button.disabled = !(isEmailCorrect() & isPasswordCorrect() & areInputFieldsNotEmpty());
}
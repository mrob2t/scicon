function saveComment(authorId, articleId) {
    const xhr = new XMLHttpRequest();
    const comment = document.getElementById("comment")
    xhr.open('POST', 'http://localhost:8080/article?author_id=' + authorId + '&article_id=' + articleId, false);
    xhr.send(comment.value);
    if (xhr.status === 200) {
        comment.value = "";
        window.location.reload(true);
    }
}
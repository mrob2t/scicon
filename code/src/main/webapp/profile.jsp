<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./styles/global.css">
    <link rel="stylesheet" href="./styles/profile.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="js/toggleDropdownMenu.js"></script>
    <title>Профиль</title>
</head>
<body>
<div class="menu" id="menu">
    <a href="${pageContext.request.contextPath}/home">Главная</a>
    <a href="${pageContext.request.contextPath}/category">Категории</a>
    <div class="account" id="account">
        <a href="${pageContext.request.contextPath}/profile" class="active">Личный кабинет</a>
    </div>
    <a href="javascript:void(0);" class="icon" onclick="toggleDropdownMenu()"><i class="fa fa-bars"></i></a>
</div>
<div class="user-info">
    <img src="./images/profile.png" width="200" alt="profile pic">
    <div class="user-description">
        <h1><%= request.getAttribute("name") + " " + request.getAttribute("lastname") %></h1>
        <p><i class="far fa-envelope" style="margin-right: 8px;"></i><%=request.getAttribute("email")%></p>
        <p><%="Статус: " + request.getAttribute("status")%></p>
        <p><a href="${pageContext.request.contextPath}/signout">Выйти из аккаунта</a></p>
        <button onclick="location.href='/new-article'" type="button">Новая статья</button>
    </div>
</div>
</body>